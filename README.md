## La gestion des tickets

Ce dépôt contient des idées de bonnes pratiques en matière d'organisation de travail.

__*Rappel :*__ 
  
Un ticket est affecté à un développeur via l'interface JIRA qui, par la suite,
permettra de mettre à jour, de commenter et de suivre un workflow de progression.

- On note deux références principales pour un ticket
    - N° JIRA (ex: 581)
    - N° CATI (ex: 104739)

### Voici un modèle de marche à suivre
*Chacune des étapes doit être validée et verrouillée avant de passer la suivante*

### Phase d'analyse
1. __Lire consciencieusement et entièrement__ la description et les commentaires du ticket.
2. Après cette première lecture, __analyser la demande dans son ensemble__ pour synthétiser et essayer de comprendre ce qui est attendu.
3. __Noter__ sur papier ou dans un fichier __toutes les interrogations__ soulevées lors de l'analyse. 
   __Ne laisser échapper aucun doute, aucune interrogation__, c'est avant de commencer le développement
qu'il faut s'assurer d'__avoir compris le besoin exprimé à 100%__.
   
### Phase d'éclaircissement
4. __Faire le point__ avec le rapporteur du ticket afin d'__aborder toutes les questions__ relevées au cours des étapes d'analyse
5. Pendant ce point, il est crucial d'__obtenir toutes les réponses__ à aux questions abordées, et de les __noter__.
   
### Phase d'élaboration du plan d'action
6. La dernière étape avant d'attaquer le développement consiste à __imaginer__ les changements que l'on va apporter au code; quels fichiers vont être impactés ? Modification, création, suppression ? Doit-on apporter des changements à la base de données ? Est-il nécessaire d'intégrer une librairie, un plugin ? etc. Attention, __toujours anticiper__ et se poser la question des __conséquences sur le code actuel__ que peuvent avoir les modifications apportées. Ajouter de nouvelles fonctionnalités __ne doit pas en perturber l'existant__, sauf indication contraire. 
   
7. Un nouveau point *intermédiaire* peut être fait afin d'exposer le plan d'action que nous avons défini.
   
### Phase de développement
8. On peut __commencer à coder__. Nous avons à présent toutes les clés en main pour aborder le développement sereinement.

### Phase de relecture et de test